;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018, 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted unit)
  #:use-module (system foreign)
  #:use-module (parted bindings)
  #:use-module (parted structs)
  #:export (SECTOR-DEFAULT-SIZE
            KILOBYTE-SIZE
            MEGABYTE-SIZE
            GIGABYTE-SIZE
            TERABYTE-SIZE
            KIBIBYTE-SIZE
            MEBIBYTE-SIZE
            GIBIBYTE-SIZE
            TEBIBYTE-SIZE

            UNIT-SECTOR
            UNIT-BYTE
            UNIT-KILOBYTE
            UNIT-MEGABYTE
            UNIT-GIGABYTE
            UNIT-TERABYTE
            UNIT-COMPACT
            UNIT-CYLINDER
            UNIT-CHS
            UNIT-PERCENT
            UNIT-KIBIBYTE
            UNIT-MEBIBYTE
            UNIT-GIBIBYTE
            UNIT-TEBIBYTE

            unit-get-size
            unit-get-name
            unit-get-by-name
            unit-set-default
            unit-get-default
            unit-format-byte
            unit-format-custom-byte
            unit-format
            unit-format-custom
            unit-parse))

(define SECTOR-DEFAULT-SIZE 512)
(define KILOBYTE-SIZE 1000)
(define MEGABYTE-SIZE 1000000)
(define GIGABYTE-SIZE 1000000000)
(define TERABYTE-SIZE 1000000000000)
(define KIBIBYTE-SIZE 1024)
(define MEBIBYTE-SIZE 1048576)
(define GIBIBYTE-SIZE 1073741824)
(define TEBIBYTE-SIZE 1099511627776)

(define UNIT-SECTOR     0)
(define UNIT-BYTE       1)
(define UNIT-KILOBYTE   2)
(define	UNIT-MEGABYTE   3)
(define	UNIT-GIGABYTE   4)
(define	UNIT-TERABYTE   5)
(define	UNIT-COMPACT    6)
(define	UNIT-CYLINDER   7)
(define	UNIT-CHS        8)
(define	UNIT-PERCENT    9)
(define	UNIT-KIBIBYTE   10)
(define	UNIT-MEBIBYTE   11)
(define	UNIT-GIBIBYTE   12)
(define	UNIT-TEBIBYTE   13)

(define unit-get-size
  (let ((proc (libparted->procedure sector-type
                                    "ped_unit_get_size"
                                    `(* ,int))))
    (lambda (device unit)
      (proc (device->pointer device) unit))))

(define unit-get-name
  (let ((proc (libparted->procedure '*
                                    "ped_unit_get_name"
                                    `(,int))))
    (lambda (unit)
      (pointer->string (proc unit)))))

(define unit-get-by-name
  (let ((proc (libparted->procedure int
                                    "ped_unit_get_by_name"
                                    '(*))))
    (lambda (unit-name)
      (proc (string->pointer unit-name)))))

(define unit-set-default
  (let ((proc (libparted->procedure void
                                    "ped_unit_set_default"
                                    `(,int))))
    (lambda (unit)
      (proc unit))))

(define unit-get-default
  (let ((proc (libparted->procedure int
                                    "ped_unit_get_default"
                                    '())))
    (lambda ()
      (proc))))

(define unit-format-byte
  (let ((proc (libparted->procedure '*
                                    "ped_unit_format_byte"
                                    `(* ,sector-type))))
    (lambda (device byte)
      (pointer->string (proc (device->pointer device) byte)))))

(define unit-format-custom-byte
  (let ((proc (libparted->procedure '*
                                    "ped_unit_format_custom_byte"
                                    `(* ,sector-type ,int))))
    (lambda (device byte unit)
      (pointer->string (proc (device->pointer device) byte unit)))))

(define unit-format
  (let ((proc (libparted->procedure '*
                                    "ped_unit_format"
                                    `(* ,sector-type))))
    (lambda (device sector)
      (pointer->string (proc (device->pointer device) sector)))))

(define unit-format-custom
  (let ((proc (libparted->procedure '*
                                    "ped_unit_format_custom"
                                    `(* ,sector-type ,int))))
    (lambda (device sector unit)
      (pointer->string (proc (device->pointer device) sector unit)))))

(define unit-parse
  (let ((proc (libparted->procedure int
                                    "ped_unit_parse"
                                    '(* * * *))))
    (lambda (input device)
      (let* ((out-range (make-double-pointer))
             (c-sector (make-c-struct (list sector-type) (list 0)))
             (result (proc (string->pointer input)
                           (device->pointer device)
                           c-sector
                           out-range)))
        (if (return-int->bool result)
            (values (car
                     (parse-c-struct c-sector (list sector-type)))
                    (pointer->geometry
                     (dereference-pointer out-range)))
            (values #f #f))))))
