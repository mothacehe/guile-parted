;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018, 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted disk)
  #:use-module (system foreign)
  #:use-module (parted bindings)
  #:use-module (parted structs)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (DISK-FLAG-CYLINDER-ALIGNMENT
            DISK-FLAG-GPT-PMBR-BOOT

            PARTITION-TYPE-NORMAL
            PARTITION-TYPE-LOGICAL
            PARTITION-TYPE-EXTENDED
            PARTITION-TYPE-FREESPACE
            PARTITION-TYPE-METADATA
            PARTITION-TYPE-PROTECTED

            PARTITION-FLAG-BOOT
            PARTITION-FLAG-ROOT
            PARTITION-FLAG-SWAP
            PARTITION-FLAG-HIDDEN
            PARTITION-FLAG-RAID
            PARTITION-FLAG-LVM
            PARTITION-FLAG-LBA
            PARTITION-FLAG-HPSERVICE
            PARTITION-FLAG-PALO
            PARTITION-FLAG-PREP
            PARTITION-FLAG-MSFT-RESERVED
            PARTITION-FLAG-BIOS-GRUB
            PARTITION-FLAG-APPLE-TV-RECOVERY
            PARTITION-FLAG-DIAG
            PARTITION-FLAG-LEGACY-BOOT
            PARTITION-FLAG-MSFT-DATA
            PARTITION-FLAG-IRST
            PARTITION-FLAG-ESP

            DISK-TYPE-FEATURE-EXTENDED
            DISK-TYPE-FEATURE-PARTITION-NAME

            disk-type-register
            disk-type-unregister
            disk-type-get-next
            disk-type-get
            disk-type-check-feature

            disk-probe
            disk-clobber
            disk-new
            disk-new-fresh
            disk-duplicate
            disk-commit
            disk-commit-to-dev
            disk-commit-to-os
            disk-check
            disk-print
            disk-get-primary-partition-count
            disk-get-last-partition-num
            disk-get-max-primary-partition-count
            disk-get-max-supported-partition-count
            disk-get-partition-alignment
            disk-set-flag
            disk-get-flag
            disk-is-flag-available?
            disk-flag-get-name
            disk-flag-get-by-name
            disk-flag-next

            partition-new
            partition-is-active?
            partition-set-flag
            partition-get-flag
            partition-is-flag-available?
            partition-set-system
            partition-set-name
            partition-get-name
            partition-is-busy?
            partition-get-path
            partition-type-get-name
            partition-type->int
            partition-flag-get-name
            partition-flag-get-by-name
            partition-flag-next
            partition-flags

            disk-add-partition
            disk-remove-partition
            disk-remove-partition*
            disk-remove-all-partitions
            disk-set-partition-geom
            disk-maximize-partition
            disk-get-max-partition-geometry
            disk-minimize-extended-partition
            disk-next-partition
            disk-get-partition
            disk-partitions
            disk-get-partition-by-sector
            disk-extended-partition
            disk-max-partition-length
            disk-max-partition-start-sector

            data-partition?
            metadata-partition?
            freespace-partition?
            normal-partition?
            extended-partition?
            logical-partition?))



;;
;; Disk destruction.
;;

(define %disk-devices
  ;; This table maps <disk> records to their "device", usually a
  ;; <device> record.  This is used to ensure that the lifetime of the
  ;; disk is shorter than that of its device so that 'disk-device'
  ;; always returns a valid object.
  (make-weak-key-hash-table))

(define %disk-destroy
  (libparted->pointer "ped_disk_destroy"))

(define* (pointer->disk! pointer #:optional device)
  (set-pointer-finalizer! pointer %disk-destroy)
  (let ((disk (pointer->disk pointer)))
    (when device
      (hashq-set! %disk-devices disk device))
    disk))


;;
;; Partition destruction.
;;

(define %partition-disks
  ;; This table maps <partition> records to their "disk", usually a
  ;; <disk> record.  This is used to ensure that the lifetime of the
  ;; partition is shorter than that of its disk so that 'partition-disk'
  ;; always returns a valid object.
  (make-weak-key-hash-table))

(define* (pointer->partition! pointer #:optional disk)
  ;; Do not set a finalizer as partitions get destroyed when the disk
  ;; object they belong to is destroyed.
  (let ((partition (pointer->partition pointer)))
    (when disk
      (hashq-set! %partition-disks partition disk))
    partition))


;;
;; Definitions.
;;

(define DISK-FLAG-CYLINDER-ALIGNMENT 1)
(define DISK-FLAG-GPT-PMBR-BOOT      2)

(define PARTITION-TYPE-NORMAL         0)
(define PARTITION-TYPE-LOGICAL        1)
(define PARTITION-TYPE-EXTENDED       2)
(define PARTITION-TYPE-FREESPACE      4)
(define PARTITION-TYPE-METADATA       8)
(define PARTITION-TYPE-PROTECTED      16)

(define PARTITION-FLAG-BOOT              1)
(define PARTITION-FLAG-ROOT              2)
(define PARTITION-FLAG-SWAP              3)
(define PARTITION-FLAG-HIDDEN            4)
(define PARTITION-FLAG-RAID              5)
(define PARTITION-FLAG-LVM               6)
(define PARTITION-FLAG-LBA               7)
(define PARTITION-FLAG-HPSERVICE         8)
(define PARTITION-FLAG-PALO              9)
(define PARTITION-FLAG-PREP              10)
(define PARTITION-FLAG-MSFT-RESERVED     11)
(define PARTITION-FLAG-BIOS-GRUB         12)
(define PARTITION-FLAG-APPLE-TV-RECOVERY 13)
(define PARTITION-FLAG-DIAG              14)
(define PARTITION-FLAG-LEGACY-BOOT       15)
(define PARTITION-FLAG-MSFT-DATA         16)
(define PARTITION-FLAG-IRST              17)
(define PARTITION-FLAG-ESP               18)

(define DISK-TYPE-FEATURE-EXTENDED       1)
(define DISK-TYPE-FEATURE-PARTITION-NAME 2)

(define disk-type-register
  (let ((proc (libparted->procedure void
                                    "ped_disk_type_register"
                                    '(*))))
    (lambda (disk-type)
      (proc (disk-type->pointer disk-type)))))

(define disk-type-unregister
  (let ((proc (libparted->procedure void
                                    "ped_disk_type_unregister"
                                    '(*))))
    (lambda (disk-type)
      (proc (disk-type->pointer disk-type)))))

(define disk-type-get-next
  (let ((proc (libparted->procedure '*
                                    "ped_disk_type_get_next"
                                    '(*))))
    (lambda (disk-type)
      (pointer->disk-type
       (proc (disk-type->pointer disk-type))))))

(define disk-type-get
  (let ((proc (libparted->procedure '*
                                    "ped_disk_type_get"
                                    '(*))))
    (lambda (name)
      (pointer->disk-type
       (proc (string->pointer name))))))

(define disk-type-check-feature
  (let ((proc (libparted->procedure int
                                    "ped_disk_type_check_feature"
                                    `(* ,int))))
    (lambda (disk-type feature)
      (let ((res (proc (disk-type->pointer disk-type) feature)))
        (eq? res 1)))))

(define disk-probe
  (let ((proc (libparted->procedure '*
                                    "ped_disk_probe"
                                    '(*))))
    (lambda (device)
      (return-ptr->type (proc (device->pointer device))
                        #:converter pointer->disk-type))))

(define disk-clobber
  (let ((proc (libparted->procedure int
                                    "ped_disk_clobber"
                                    '(*))))
    (lambda (device)
      (proc (device->pointer device)))))

(define disk-new
  (let* ((proc (libparted->procedure '*
                                     "ped_disk_new"
                                     '(*))))
    (lambda (device)
      (let ((pointer (proc (device->pointer device))))
        (and (not (eq? pointer %null-pointer))
             (pointer->disk! pointer device))))))

(define disk-new-fresh
  (let* ((proc (libparted->procedure '*
                                     "ped_disk_new_fresh"
                                     '(* *))))
    (lambda (device disk-type)
      (let ((pointer (proc (device->pointer device)
                           (disk-type->pointer disk-type))))
        (and (not (eq? pointer %null-pointer))
             (pointer->disk! pointer device))))))

(define disk-duplicate
  (let ((proc (libparted->procedure '*
                                    "ped_disk_duplicate"
                                    '(*))))
    (lambda (disk)
      (pointer->disk!
       (proc (disk->pointer disk))
       (disk-device disk)))))

(define disk-commit
  (let ((proc (libparted->procedure int
                                    "ped_disk_commit"
                                    '(*))))
    (lambda (disk)
      (return-int->bool
       (proc (disk->pointer disk))))))

(define disk-commit-to-dev
  (let ((proc (libparted->procedure int
                                    "ped_disk_commit_to_dev"
                                    '(*))))
    (lambda (disk)
      (return-int->bool
       (proc (disk->pointer disk))))))

(define disk-commit-to-os
  (let ((proc (libparted->procedure int
                                    "ped_disk_commit_to_os"
                                    '(*))))
    (lambda (disk)
      (return-int->bool
       (proc (disk->pointer disk))))))

(define disk-check
  (let ((proc (libparted->procedure int
                                    "ped_disk_check"
                                    '(*))))
    (lambda (disk)
      (proc (disk->pointer disk)))))

(define disk-print
  (let ((proc (libparted->procedure void
                                    "ped_disk_print"
                                    '(*))))
    (lambda (disk)
      (proc (disk->pointer disk)))))

(define disk-get-primary-partition-count
  (let ((proc (libparted->procedure int
                                    "ped_disk_get_primary_partition_count"
                                    '(*))))
    (lambda (disk)
      (proc (disk->pointer disk)))))

(define disk-get-last-partition-num
  (let ((proc (libparted->procedure int
                                    "ped_disk_get_last_partition_num"
                                    '(*))))
    (lambda (disk)
      (proc (disk->pointer disk)))))

(define disk-get-max-primary-partition-count
  (let ((proc (libparted->procedure int
                                    "ped_disk_get_max_primary_partition_count"
                                    '(*))))
    (lambda (disk)
      (proc (disk->pointer disk)))))

(define disk-get-max-supported-partition-count
  (let ((proc
         (libparted->procedure int
                               "ped_disk_get_max_supported_partition_count"
                               '(* *))))
    (lambda (disk)
      (let* ((c-supported (make-c-struct (list int) (list 0)))
             (res (proc (disk->pointer disk) c-supported)))
        (car (parse-c-struct c-supported (list int)))))))

(define disk-get-partition-alignment
  (let ((proc (libparted->procedure '*
                                    "ped_disk_get_partition_alignment"
                                    '(*))))
    (lambda (disk)
      (pointer->alignment
       (proc (disk->pointer disk))))))

(define disk-set-flag
  (let ((proc (libparted->procedure int
                                    "ped_disk_set_flag"
                                    `(* ,int ,int))))
    (lambda (disk flag state)
      (proc (disk->pointer disk) flag state))))

(define disk-get-flag
  (let ((proc (libparted->procedure int
                                    "ped_disk_get_flag"
                                    `(* ,int))))
    (lambda (disk flag)
      (proc (disk->pointer disk) flag))))

(define disk-is-flag-available?
  (let* ((proc (libparted->procedure int
                                     "ped_disk_is_flag_available"
                                     `(* ,int))))
    (lambda (disk flag)
      (let ((res (proc (disk->pointer disk) flag)))
        (eq? res 1)))))

(define disk-flag-get-name
  (let ((proc (libparted->procedure '*
                                    "ped_disk_flag_get_name"
                                    `(,int))))
    (lambda (flag)
      (pointer->string (proc flag)))))

(define disk-flag-get-by-name
  (let ((proc (libparted->procedure int
                                    "ped_disk_flag_get_by_name"
                                    '(*))))
    (lambda (name)
      (proc (string->pointer name)))))

(define disk-flag-next
  (let ((proc (libparted->procedure int
                                    "ped_disk_flag_next"
                                    `(,int))))
    (lambda (flag)
      (proc flag))))

(define partition-new
  (let ((proc
         (libparted->procedure '*
                               "ped_partition_new"
                               `(* ,int * ,sector-type ,sector-type))))
    (lambda* (disk #:key
                   type
                   filesystem-type
                   start end)
      (pointer->partition!
       (proc (disk->pointer disk)
             type
             (filesystem-type->pointer filesystem-type)
             start end)))))

(define partition-is-active?
  (let* ((proc (libparted->procedure int
                                     "ped_partition_is_active"
                                     '(*))))
    (lambda (partition)
      (let ((res (proc (partition->pointer partition))))
        (eq? res 1)))))

(define partition-set-flag
  (let ((proc (libparted->procedure int
                                    "ped_partition_set_flag"
                                    `(* ,int ,int))))
    (lambda (partition flag state)
      (proc (partition->pointer partition)
            flag
            state))))

(define partition-get-flag
  (let ((proc (libparted->procedure int
                                    "ped_partition_get_flag"
                                    `(* ,int))))
    (lambda (partition flag)
      (return-int->bool
       (proc (partition->pointer partition)
             flag)))))

(define partition-is-flag-available?
  (let* ((proc (libparted->procedure int
                                     "ped_partition_is_flag_available"
                                     `(* ,int))))
    (lambda (partition flag)
      (let ((res (proc (partition->pointer partition)
                       flag)))
        (eq? res 1)))))

(define partition-set-system
  (let ((proc (libparted->procedure int
                                    "ped_partition_set_system"
                                    '(* *))))
    (lambda (partition filesystem-type)
      (proc (partition->pointer partition)
            (filesystem-type->pointer filesystem-type)))))

(define partition-set-name
  (let ((proc (libparted->procedure int
                                    "ped_partition_set_name"
                                    '(* *))))
    (lambda (partition name)
      (proc (partition->pointer partition)
            (string->pointer name)))))

(define partition-get-name
  (let ((proc (libparted->procedure '*
                                    "ped_partition_get_name"
                                    '(*))))
    (lambda (partition)
      (pointer->string
       (proc (partition->pointer partition))))))

(define partition-is-busy?
  (let* ((proc (libparted->procedure '*
                                     "ped_partition_is_busy"
                                     '(*))))
    (lambda (partition)
      (let ((res (proc (partition->pointer partition))))
        (eq? res 1)))))

(define partition-get-path
  (let ((proc (libparted->procedure '*
                                    "ped_partition_get_path"
                                    '(*))))
    (lambda (partition)
      (pointer->string
       (proc (partition->pointer partition))))))

(define partition-type-get-name
  (let ((proc (libparted->procedure '*
                                    "ped_partition_type_get_name"
                                    `(,int))))
    (lambda (partition-type)
      (let ((type (partition-type->int partition-type)))
        (pointer->string (proc type))))))

(define partition-flag-get-name
  (let ((proc (libparted->procedure '*
                                    "ped_partition_flag_get_name"
                                    `(,int))))
    (lambda (partition-flag)
      (pointer->string (proc partition-flag)))))

(define partition-flag-get-by-name
  (let ((proc (libparted->procedure int
                                    "ped_partition_flag_get_by_name"
                                    '(*))))
    (lambda (name)
      (proc (string->pointer name)))))

(define partition-flag-next
  (let ((proc (libparted->procedure int
                                    "ped_partition_flag_next"
                                    `(,int))))
    (lambda (partition-flag)
      (proc partition-flag))))

(define (partition-flags partition)
  (let loop ((flag (partition-flag-next 0)))
    (if (eq? flag 0)
        '()
        (cons flag
              (loop (partition-flag-next flag))))))

(define (partition-type->int partition-type)
  (case partition-type
    ((normal)     0)
    ((logical)    1)
    ((extended)   2)
    ((free-space) 4)
    ((metadata)   8)
    ((protected)  16)))

(define disk-add-partition
  (let* ((proc (libparted->procedure int
                                    "ped_disk_add_partition"
                                    '(* * *))))
    (lambda (disk partition constraint)
      (let ((res (proc (disk->pointer disk)
                    (partition->pointer partition)
                    (constraint->pointer constraint))))
        (eq? res 1)))))

(define disk-remove-partition
  ;; Remove the given PARTITION from DISK. If PARTITION is an extended
  ;; partition, it must not contain any logical partitions. This does
  ;; not destroy the partition.
  (let ((proc (libparted->procedure int
                                    "ped_disk_remove_partition"
                                    '(* *))))
    (lambda (disk partition)
      (proc (disk->pointer disk)
            (partition->pointer partition)))))

(define (disk-remove-partition* disk partition)
  "Wrap disk-remove-partition to remove PARTITION and all its logical
partitions if it is an extended partition. Also prevent from trying to
remove a non-data partition (freespace, metadata or protected types)."
  (let ((partitions (disk-partitions disk)))
    (when (extended-partition? partition)
      (for-each (lambda (p)
                  (and (data-partition? p)
                       (disk-remove-partition disk p)))
                (filter logical-partition? partitions)))
    (when (data-partition? partition)
      (disk-remove-partition disk partition))))

(define (disk-remove-all-partitions disk)
  "Remove all partitions from given DISK."
  (match (filter data-partition?
                 (disk-partitions disk))
    (() #t)
    ((p rest ...)
     (disk-remove-partition* disk p)
     (disk-remove-all-partitions disk))))

(define disk-set-partition-geom
  (let ((proc (libparted->procedure int
                                    "ped_disk_set_partition_geom"
                                    `(* * * ,sector-type ,sector-type))))
    (lambda* (disk partition
                   #:key
                   constraint
                   start end)
      (proc (disk->pointer disk)
            (partition->pointer partition)
            (constraint->pointer constraint)
            start end))))

(define disk-maximize-partition
  (let ((proc (libparted->procedure int
                                    "ped_disk_maximize_partition"
                                    '(* * *))))
    (lambda (disk partition constraint)
      (proc (disk->pointer disk)
            (partition->pointer partition)
            (constraint->pointer constraint)))))

(define disk-get-max-partition-geometry
  (let ((proc (libparted->procedure '*
                                    "ped_disk_get_max_partition_geometry"
                                    '(* * *))))
    (lambda (disk partition constraint)
      (pointer->geometry
       (proc (disk->pointer disk)
             (partition->pointer partition)
             (constraint->pointer constraint))))))

(define disk-minimize-extended-partition
  (let ((proc (libparted->procedure int
                                    "ped_disk_minimize_extended_partition"
                                    '(*))))
    (lambda (disk)
      (proc (disk->pointer disk)))))

(define disk-next-partition
  (let ((proc (libparted->procedure '*
                                    "ped_disk_next_partition"
                                    '(* *))))
    (lambda* (disk
              #:key (partition #f))
      (let ((pointer (proc (disk->pointer disk)
                        (if partition
                            (partition->pointer partition)
                            %null-pointer))))
        (and (not (eq? pointer %null-pointer))
             (pointer->partition pointer))))))

(define disk-get-partition
  (let ((proc (libparted->procedure '*
                                    "ped_disk_get_partition"
                                    `(* ,int))))
    (lambda (disk number)
      (return-ptr->type (proc (disk->pointer disk) number)
                        #:converter pointer->partition))))

(define (disk-partitions disk)
  (let loop ((partition (disk-next-partition disk)))
    (if (not partition)
        '()
        (cons partition
              (loop (disk-next-partition disk
                                         #:partition
                                         partition))))))

(define disk-get-partition-by-sector
  (let ((proc (libparted->procedure '*
                                    "ped_disk_get_partition_by_sector"
                                    `(* ,sector-type))))
    (lambda (disk sector)
      (pointer->partition
       (proc (disk->pointer disk) sector)))))

(define disk-extended-partition
  (let ((proc (libparted->procedure '*
                                    "ped_disk_extended_partition"
                                    '(*))))
    (lambda (disk)
      (return-ptr->type (proc (disk->pointer disk))
                        #:converter pointer->partition))))

(define disk-max-partition-length
  (let ((proc (libparted->procedure sector-type
                                    "ped_disk_max_partition_length"
                                    '(*))))
    (lambda (disk)
      (proc (disk->pointer disk)))))

(define* disk-max-partition-start-sector
  (let ((proc (libparted->procedure sector-type
                                    "ped_disk_max_partition_start_sector"
                                    '(*))))
    (lambda (disk)
      (proc (disk->pointer disk)))))

(define (data-partition? partition)
  "Return #t if PARTITION is a partition dedicated to data (by opposition to
freespace, metadata and protected partition types), return #f otherwise."
  (let ((type (partition-type partition)))
    (not (any (lambda (flag)
                (member flag type))
              '(free-space metadata protected)))))

(define (metadata-partition? partition)
  "Return #t if PARTITION is a metadata partition, #f otherwise."
  (let ((type (partition-type partition)))
    (member 'metadata type)))

(define (freespace-partition? partition)
  "Return #t if PARTITION is a free-space partition, #f otherwise."
  (let ((type (partition-type partition)))
    (member 'free-space type)))

(define (normal-partition? partition)
  "return #t if partition is a normal partition, #f otherwise."
  (let ((type (partition-type partition)))
    (member 'normal type)))

(define (extended-partition? partition)
  "return #t if partition is an extended partition, #f otherwise."
  (let ((type (partition-type partition)))
    (member 'extended type)))

(define (logical-partition? partition)
  "Return #t if PARTITION is a logical partition, #f otherwise."
  (let ((type (partition-type partition)))
    (member 'logical type)))
