;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018, 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted natmath)
  #:use-module (system foreign)
  #:use-module (parted bindings)
  #:use-module (parted structs)
  #:export (pointer->alignment!
            round-up-to
            round-down-to
            round-to-nearest
            greatest-common-divisor
            alignment-init
            alignment-new
            alignment-duplicate
            alignment-intersect
            alignment-align-up
            alignment-align-down
            alignment-align-nearest
            alignment-is-aligned?
            alignment-any
            alignment-none
            div-round-up
            div-round-to-nearest))

(define %alignment-destroy
  (libparted->pointer "ped_alignment_destroy"))

(define (pointer->alignment! pointer)
  (set-pointer-finalizer! pointer %alignment-destroy)
  (pointer->alignment pointer))

(define round-up-to
  (let ((proc (libparted->procedure sector-type
                                    "ped_round_up_to"
                                    `(,sector-type ,sector-type))))
    (lambda (sector grain-size)
      (proc sector grain-size))))

(define round-down-to
  (let ((proc (libparted->procedure sector-type
                                    "ped_round_down_to"
                                    `(,sector-type ,sector-type))))
    (lambda (sector grain-size)
      (proc sector grain-size))))

(define round-to-nearest
  (let ((proc (libparted->procedure sector-type
                                    "ped_round_to_nearest"
                                    `(,sector-type ,sector-type))))
    (lambda (sector grain-size)
      (proc sector grain-size))))

(define greatest-common-divisor
  (let ((proc (libparted->procedure sector-type
                                    "ped_greatest_common_divisor"
                                    `(,sector-type ,sector-type))))
    (lambda (sector-a sector-b)
      (proc sector-a sector-b))))

(define alignment-init
  (let ((proc (libparted->procedure int
                                    "ped_alignment_init"
                                    `(* ,sector-type ,sector-type))))
    (lambda (alignment offset grain-size)
      (proc (alignment->pointer alignment) offset grain-size))))

(define alignment-new
  (let ((proc (libparted->procedure '*
                                    "ped_alignment_new"
                                    `(,sector-type ,sector-type))))
    (lambda (offset grain-size)
      (pointer->alignment! (proc offset grain-size)))))

(define alignment-duplicate
  (let ((proc (libparted->procedure '*
                                    "ped_alignment_duplicate"
                                    '(*))))
    (lambda (alignment)
      (pointer->alignment!
       (proc (alignment->pointer alignment))))))

(define alignment-intersect
  (let ((proc (libparted->procedure '*
                                    "ped_alignment_intersect"
                                    '(* *))))
    (lambda (alignment-a alignment-b)
      (pointer->alignment!
       (proc (alignment->pointer alignment-a)
             (alignment->pointer alignment-b))))))

(define alignment-align-up
  (let ((proc (libparted->procedure sector-type
                                    "ped_alignment_align_up"
                                    `(* * ,sector-type))))
    (lambda (alignment geometry sector)
      (proc (alignment->pointer alignment)
            (geometry->pointer geometry)
            sector))))

(define alignment-align-down
  (let ((proc (libparted->procedure sector-type
                                    "ped_alignment_align_down"
                                    `(* * ,sector-type))))
    (lambda (alignment geometry sector)
      (proc (alignment->pointer alignment)
            (geometry->pointer geometry)
            sector))))

(define alignment-align-nearest
  (let ((proc (libparted->procedure sector-type
                                    "ped_alignment_align_nearest"
                                    `(* * ,sector-type))))
    (lambda (alignment geometry sector)
      (proc (alignment->pointer alignment)
            (geometry->pointer geometry)
            sector))))

(define alignment-is-aligned?
  (let* ((proc (libparted->procedure int
                                     "ped_alignment_is_aligned"
                                     `(* * ,sector-type))))
    (lambda (alignment geometry sector)
      (let ((res (proc (alignment->pointer alignment)
                    (geometry->pointer geometry)
                    sector)))
        (eq? res 1)))))

(define (alignment-any)
  (dereference-pointer
   (libparted->pointer "ped_alignment_any")))

(define (alignment-none)
  (dereference-pointer
   (libparted->pointer "ped_alignment_none")))

(define div-round-up
  (let ((proc (libparted->procedure sector-type
                                    "ped_div_round_up"
                                    `(,sector-type ,sector-type))))
    (lambda (numerator divisor)
      (proc numerator divisor))))

(define div-round-to-nearest
  (let ((proc (libparted->procedure sector-type
                                    "ped_div_round_to_nearest"
                                    `(,sector-type ,sector-type))))
    (lambda (numerator divisor)
      (proc numerator divisor))))
