;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted structs)
  #:use-module (parted bindings)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module ((system foreign) #:prefix guile:)
  #:use-module (bytestructures guile)
  #:use-module (ice-9 match)
  #:export (<device>
            make-device
            device?
            device->pointer
            pointer->device
            device-model
            device-path
            device-raw-type
            device-read-only?
            device-dirty?
            device-boot-dirty?
            device-length
            device-sector-size
            device-phys-sector-size

            <geometry>
            make-geometry
            geometry->pointer
            pointer->geometry
            geometry-bytestructure
            geometry-start
            geometry-end
            geometry-length
            display-geometry

            <alignment>
            make-alignment
            alignment->pointer
            pointer->alignment
            alignment-bytestructure
            alignment-offset
            alignment-grain-size

            <constraint>
            make-constraint
            constraint->pointer
            pointer->constraint
            display-constraint

            <exception>
            make-exception
            exception->pointer
            pointer->exception

            <filesystem-type>
            make-filesystem-type
            filesystem-type->pointer
            pointer->filesystem-type
            filesystem-type-name

            <filesystem-alias>
            make-filesystem-alias
            filesystem-alias->pointer
            pointer->filesystem-alias

            <filesystem>
            make-filesystem
            filesystem->pointer
            pointer->filesystem

            <timer>
            make-timer
            timer->pointer
            pointer->timer

            <partition>
            make-partition
            partition?
            partition->pointer
            pointer->partition
            partition-bytestructure
            partition-number
            partition-start
            partition-end
            partition-length
            partition-geom
            partition-fs-type
            partition-type
            partition-prev
            partition-next
            partition-disk

            <disk>
            disk?
            make-disk
            disk->pointer
            pointer->disk
            disk-disk-type
            disk-device

            <disk-type>
            make-disk-type
            disk-type->pointer
            pointer->disk-type
            disk-type-name

            make-double-pointer
            sector-type))

(define bytestructure->pointer
  (compose guile:bytevector->pointer
           bytestructure-bytevector))

(define (pointer->bytestructure pointer struct)
  (make-bytestructure
   (guile:pointer->bytevector
    pointer
    (bytestructure-descriptor-size struct))
   0
   struct))

(define (make-double-pointer)
  (guile:bytevector->pointer (make-bytevector (guile:sizeof '*))))

(define sector-type guile:int64)

(define time-type int)

(define (flags->symbols flags map-list)
  (fold (lambda (flag-map symbols)
          (match flag-map
            ((flag symbol)
             (if (> (logand flag flags) 0)
                 (cons symbol symbols)
                 symbols))))
        '()
        map-list))

;; PedCHSGeometry
(define %chs-geometry
  (bs:struct `((cylinders ,int)
               (heads ,int)
               (sectors ,int))))

;; PedSector
(define %sector long-long)

;; PedDeviceType
(define %device-type int)

;; PedDevice
(define %device
  (bs:struct `((next ,(bs:pointer uint8))
               (model ,(bs:pointer uint8))
               (path ,(bs:pointer uint8))
               (device-type ,%device-type)
               (sector-size ,long-long)
               (phys-sector-size ,long-long)
               (length ,%sector)
               (open-count ,int)
               (read-only ,int)
               (external-mode ,int)
               (dirty ,int)
               (boot-dirty ,int)
               (hw-geom ,%chs-geometry)
               (bios-geom ,%chs-geometry)
               (host ,short)
               (did ,short)
               (arch-specific ,(bs:pointer 'void)))))

;; PedGeometry
(define %geometry
  (bs:struct `((dev ,(bs:pointer uint8))
               (start ,%sector)
               (length ,%sector)
               (end ,%sector))))

;; PedAlignment
(define %alignment
  (bs:struct `((offset ,%sector)
               (grain-size ,%sector))))

;; PedConstraint
(define %constraint
  (bs:struct `((start-align ,(bs:pointer %alignment))
               (end-align ,(bs:pointer %alignment))
               (start-range ,(bs:pointer %geometry))
               (end-range ,(bs:pointer %geometry))
               (min-size ,%sector)
               (max-size ,%sector))))

;; PedException
(define %exception
  (bs:struct `((message ,(bs:pointer uint8))
               (type ,int)
               (option ,int))))

;; PedFileSystemType
(define %filesystem-type
  (bs:struct `((next ,(bs:pointer uint8))
               (name ,(bs:pointer uint8))
               (ops ,(bs:pointer uint8)))))

;; PedFileSystemAlias
(define %filesystem-alias
  (bs:struct `((next ,(bs:pointer uint8))
               (fs-type ,(bs:pointer uint8))
               (alias ,(bs:pointer uint8))
               (deprecated ,int))))

;; PedFileSystem
(define %filesystem
  (bs:struct `((next ,(bs:pointer uint8))
               (geom ,(bs:pointer uint8))
               (checked ,int)
               (type-specific ,(bs:pointer uint8)))))

;; PedTimer
(define %timer
  (bs:struct `((frac ,float)
               (start ,time-type)
               (now ,time-type)
               (predicted-end ,time-type)
               (state-name ,(bs:pointer uint8))
               (handler ,(bs:pointer uint8))
               (context ,(bs:pointer 'void)))))

;; PedPartition
(define %partition
  (bs:struct `((prev ,(bs:pointer uint8))
               (next ,(bs:pointer uint8))
               (disk ,(bs:pointer uint8))
               (geom ,%geometry)
               (num ,int)
               (type ,int)
               (fs-type ,(bs:pointer %filesystem-type))
               (part-list ,(bs:pointer uint8))
               (disk-specific ,(bs:pointer 'void)))))

;; PedDisk
(define %disk
  (bs:struct `((dev ,(bs:pointer uint8))
               (type ,(bs:pointer uint8))
               (block-sizes ,(bs:pointer uint8))
               (part-list ,(bs:pointer uint8))
               (disk-specific ,(bs:pointer uint8))
               (needs-clobber ,int)
               (update-mode ,int))))

;; PedDiskType
(define %disk-type
  (bs:struct `((next ,(bs:pointer uint8))
               (name ,(bs:pointer uint8))
               (ops ,(bs:pointer uint8))
               (features ,int))))


;;
;; Device.
;;

(define-record-type <device>
  (%make-device bytestructure)
  device?
  (bytestructure device-bytestructure))

(define (make-device)
  (%make-device (bytestructure %device)))

(define (device->pointer device)
  (bytestructure->pointer (device-bytestructure device)))

(define (pointer->device pointer)
  (%make-device (pointer->bytestructure pointer %device)))

(define (device-model device)
  (guile:pointer->string
   (guile:make-pointer
    (bytestructure-ref (device-bytestructure device) 'model))))

(define (device-path device)
  (guile:pointer->string
   (guile:make-pointer
    (bytestructure-ref (device-bytestructure device) 'path))))

(define (device-raw-type device)
  (bytestructure-ref (device-bytestructure device) 'device-type))

(define (device-read-only? device)
  (eq? (bytestructure-ref (device-bytestructure device) 'read-only) 1))

(define (device-dirty? device)
  (eq? (bytestructure-ref (device-bytestructure device) 'dirty) 1))

(define (device-boot-dirty? device)
  (eq? (bytestructure-ref (device-bytestructure device) 'boot-dirty) 1))

(define (device-length device)
  (bytestructure-ref (device-bytestructure device) 'length))

(define (device-sector-size device)
  (bytestructure-ref (device-bytestructure device) 'sector-size))

(define (device-phys-sector-size device)
  (bytestructure-ref (device-bytestructure device) 'phys-sector-size))


;;
;; Geometry.
;;

(define-record-type <geometry>
  (%make-geometry bytestructure)
  geometry?
  (bytestructure geometry-bytestructure))

(define (make-geometry)
  (%make-geometry (bytestructure %geometry)))

(define (geometry->pointer geometry)
  (bytestructure->pointer (geometry-bytestructure geometry)))

(define (pointer->geometry pointer)
  (%make-geometry (pointer->bytestructure pointer %geometry)))

(define (geometry-start geometry)
  (bytestructure-ref (geometry-bytestructure geometry) 'start))

(define (geometry-end geometry)
  (bytestructure-ref (geometry-bytestructure geometry) 'end))

(define (geometry-length geometry)
  (bytestructure-ref (geometry-bytestructure geometry) 'length))

(define (display-geometry geometry)
  (pk (list
       (geometry-start geometry)
       (geometry-end geometry))))


;;
;; Alignment.
;;

(define-record-type <alignment>
  (%make-alignment bytestructure)
  alignment?
  (bytestructure alignment-bytestructure))

(define (make-alignment)
  (%make-alignment (bytestructure %alignment)))

(define (alignment->pointer alignment)
  (bytestructure->pointer (alignment-bytestructure alignment)))

(define (pointer->alignment pointer)
  (%make-alignment (pointer->bytestructure pointer %alignment)))

(define (alignment-offset alignment)
  (bytestructure-ref (alignment-bytestructure alignment) 'offset))

(define (alignment-grain-size alignment)
  (bytestructure-ref (alignment-bytestructure alignment) 'grain-size))


;;
;; Constraint.
;;

(define-record-type <constraint>
  (%make-constraint bytestructure)
  constraint?
  (bytestructure constraint-bytestructure))

(define (make-constraint)
  (%make-constraint (bytestructure %constraint)))

(define (constraint->pointer constraint)
  (bytestructure->pointer (constraint-bytestructure constraint)))

(define (pointer->constraint pointer)
  (%make-constraint (pointer->bytestructure pointer %constraint)))

(define (display-constraint constraint)
  (let ((start-align (pointer->alignment
                      (guile:make-pointer
                       (bytestructure-ref
                        (constraint-bytestructure constraint)
                        'start-align))))
        (end-align (pointer->alignment
                    (guile:make-pointer
                     (bytestructure-ref
                      (constraint-bytestructure constraint)
                      'end-align))))
        (start-range (pointer->geometry
                      (guile:make-pointer
                       (bytestructure-ref
                        (constraint-bytestructure constraint)
                        'start-range))))
        (end-range (pointer->geometry
                      (guile:make-pointer
                       (bytestructure-ref
                        (constraint-bytestructure constraint)
                        'end-range))))
        (min-size (bytestructure-ref (constraint-bytestructure constraint) 'min-size))
        (max-size (bytestructure-ref (constraint-bytestructure constraint) 'max-size)))
    (pk (list
         (alignment-offset start-align)
         (alignment-grain-size start-align)
         (alignment-offset end-align)
         (alignment-grain-size end-align)
         (geometry-start start-range)
         (geometry-end start-range)
         (geometry-start end-range)
         (geometry-end end-range)
         min-size max-size))
    constraint))

;;
;; Exception.
;;

(define-record-type <exception>
  (%make-exception bytestructure)
  exception?
  (bytestructure exception-bytestructure))

(define (make-exception)
  (%make-exception (bytestructure %exception)))

(define (exception->pointer exception)
  (bytestructure->pointer (exception-bytestructure exception)))

(define (pointer->exception pointer)
  (%make-exception (pointer->bytestructure pointer %exception)))


;;
;; Filesystem type.
;;

(define-record-type <filesystem-type>
  (%make-filesystem-type bytestructure)
  filesystem-type?
  (bytestructure filesystem-type-bytestructure))

(define (make-filesystem-type)
  (%make-filesystem-type (bytestructure %filesystem-type)))

(define (filesystem-type->pointer filesystem-type)
  (bytestructure->pointer (filesystem-type-bytestructure filesystem-type)))

(define (pointer->filesystem-type pointer)
  (%make-filesystem-type (pointer->bytestructure pointer %filesystem-type)))

(define (filesystem-type-name filesystem-type)
  (guile:pointer->string
   (guile:make-pointer
    (bytestructure-ref (filesystem-type-bytestructure filesystem-type) 'name))))


;;
;; Filesystem alias.
;;

(define-record-type <filesystem-alias>
  (%make-filesystem-alias bytestructure)
  filesystem-alias?
  (bytestructure filesystem-alias-bytestructure))

(define (make-filesystem-alias)
  (%make-filesystem-alias (bytestructure %filesystem-alias)))

(define (filesystem-alias->pointer filesystem-alias)
  (bytestructure->pointer (filesystem-alias-bytestructure filesystem-alias)))

(define (pointer->filesystem-alias pointer)
  (%make-filesystem-alias (pointer->bytestructure pointer %filesystem-alias)))


;;
;; Filesystem.
;;

(define-record-type <filesystem>
  (%make-filesystem bytestructure)
  filesystem?
  (bytestructure filesystem-bytestructure))

(define (make-filesystem)
  (%make-filesystem (bytestructure %filesystem)))

(define (filesystem->pointer filesystem)
  (bytestructure->pointer (filesystem-bytestructure filesystem)))

(define (pointer->filesystem pointer)
  (%make-filesystem (pointer->bytestructure pointer %filesystem)))



;;
;; Timer.
;;

(define-record-type <timer>
  (%make-timer bytestructure)
  timer?
  (bytestructure timer-bytestructure))

(define (make-timer)
  (%make-timer (bytestructure %timer)))

(define (timer->pointer timer)
  (bytestructure->pointer (timer-bytestructure timer)))

(define (pointer->timer pointer)
  (%make-timer (pointer->bytestructure pointer %timer)))


;;
;; Partition.
;;

(define-record-type <partition>
  (%make-partition bytestructure)
  partition?
  (bytestructure partition-bytestructure))

(define (make-partition)
  (%make-partition (bytestructure %partition)))

(define (partition->pointer partition)
  (bytestructure->pointer (partition-bytestructure partition)))

(define (pointer->partition pointer)
  (%make-partition (pointer->bytestructure pointer %partition)))

(define (partition-number partition)
  (bytestructure-ref (partition-bytestructure partition) 'num))

(define (partition-start partition)
  (bytestructure-ref
   (bytestructure-ref (partition-bytestructure partition) 'geom)
   'start))

(define (partition-end partition)
  (bytestructure-ref
   (bytestructure-ref (partition-bytestructure partition) 'geom)
   'end))

(define (partition-length partition)
  (bytestructure-ref
   (bytestructure-ref (partition-bytestructure partition) 'geom)
   'length))

(define (partition-geom partition)
  (%make-geometry
   (bytestructure-ref (partition-bytestructure partition) 'geom)))

(define (partition-fs-type partition)
  (let ((res (bytestructure-ref
              (partition-bytestructure partition)
              'fs-type)))
    (and (not (eq? res 0))
         (pointer->filesystem-type (guile:make-pointer res)))))

(define (partition-type partition)
  (let ((type (bytestructure-ref
               (partition-bytestructure partition)
               'type)))
    (if (eq? type 0)
        (list 'normal)
        (flags->symbols type
                        '((1  logical)
                          (2  extended)
                          (4  free-space)
                          (8  metadata)
                          (16 protected))))))
(define (partition-prev partition)
  (return-ptr->type
   (guile:make-pointer
    (bytestructure-ref (partition-bytestructure partition) 'prev))
   #:converter pointer->partition))

(define (partition-next partition)
  (return-ptr->type
   (guile:make-pointer
    (bytestructure-ref (partition-bytestructure partition) 'next))
   #:converter pointer->partition))

(define (partition-disk partition)
  (pointer->disk
   (guile:make-pointer
    (bytestructure-ref (partition-bytestructure partition) 'disk))))


;;
;; Disk.
;;

(define-record-type <disk>
  (%make-disk bytestructure)
  disk?
  (bytestructure disk-bytestructure))

(define (make-disk)
  (%make-disk (bytestructure %disk)))

(define (disk->pointer disk)
  (bytestructure->pointer (disk-bytestructure disk)))

(define (pointer->disk pointer)
  (%make-disk (pointer->bytestructure pointer %disk)))

(define (disk-disk-type disk)
  (pointer->disk-type
   (guile:make-pointer
    (bytestructure-ref (disk-bytestructure disk) 'type))))

(define (disk-device disk)
  (pointer->device
   (guile:make-pointer
    (bytestructure-ref (disk-bytestructure disk) 'dev))))


;;
;; Disk type.
;;

(define-record-type <disk-type>
  (%make-disk-type bytestructure)
  disk-type?
  (bytestructure disk-type-bytestructure))

(define (make-disk-type)
  (%make-disk-type (bytestructure %disk-type)))

(define (disk-type->pointer disk-type)
  (bytestructure->pointer (disk-type-bytestructure disk-type)))

(define (pointer->disk-type pointer)
  (%make-disk-type (pointer->bytestructure pointer %disk-type)))

(define (disk-type-name disk-type)
  (guile:pointer->string
   (guile:make-pointer
    (bytestructure-ref (disk-type-bytestructure disk-type) 'name))))
