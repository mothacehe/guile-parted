;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018, 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted geom)
  #:use-module (system foreign)
  #:use-module (parted bindings)
  #:use-module (parted structs)
  #:export (geometry-init
            geometry-new
            geometry-duplicate
            geometry-intersect
            geometry-set
            geometry-set-start
            geometry-set-end
            geometry-test-overlap?
            geometry-test-inside?
            geometry-test-equal?
            geometry-test-sector-inside?))

(define %geometry-destroy
  (libparted->pointer "ped_geometry_destroy"))

(define (pointer->geometry! pointer)
  (set-pointer-finalizer! pointer %geometry-destroy)
  (pointer->geometry pointer))

(define geometry-init
  (let ((proc (libparted->procedure int
                                    "ped_geometry_init"
                                    `(* * ,sector-type ,sector-type))))
    (lambda* (geometry device #:key start length)
      (proc (geometry->pointer geometry) (device->pointer device)
            start length))))

(define geometry-new
  (let ((proc (libparted->procedure '*
                                    "ped_geometry_new"
                                    `(* ,sector-type ,sector-type))))
    (lambda* (device #:key start length)
      (pointer->geometry! (proc (device->pointer device) start length)))))

(define geometry-duplicate
  (let ((proc (libparted->procedure '*
                                    "ped_geometry_duplicate"
                                    '(*))))
    (lambda (geometry)
      (pointer->geometry! (proc (geometry->pointer geometry))))))

(define geometry-intersect
  (let ((proc (libparted->procedure '*
                                    "ped_geometry_intersect"
                                    '(* *))))
    (lambda (geometry-a geometry-b)
      (pointer->geometry! (proc (geometry->pointer geometry-a)
                                (geometry->pointer geometry-b))))))

(define geometry-set
  (let ((proc (libparted->procedure int
                                    "ped_geometry_set"
                                    `(* ,sector-type ,sector-type))))
    (lambda* (geometry #:key start length)
      (proc (geometry->pointer geometry) start length))))

(define geometry-set-start
  (let ((proc (libparted->procedure int
                                    "ped_geometry_set_start"
                                    `(* ,sector-type))))
    (lambda (geometry start)
      (proc (geometry->pointer geometry) start))))

(define geometry-set-end
  (let ((proc (libparted->procedure int
                                    "ped_geometry_set_end"
                                    `(* ,sector-type))))
    (lambda (geometry end)
      (proc (geometry->pointer geometry) end))))

(define geometry-test-overlap?
  (let* ((proc (libparted->procedure int
                                     "ped_geometry_test_overlap"
                                     '(* *))))
    (lambda (geometry-a geometry-b)
      (let ((res (proc (geometry->pointer geometry-a)
                       (geometry->pointer geometry-b))))
        (eq? res 1)))))

(define geometry-test-inside?
  (let* ((proc (libparted->procedure int
                                     "ped_geometry_test_inside"
                                     '(* *))))
    (lambda (geometry-a geometry-b)
      (let ((res (proc (geometry->pointer geometry-a)
                       (geometry->pointer geometry-b))))
        (eq? res 1)))))

(define geometry-test-equal?
  (let* ((proc (libparted->procedure int
                                     "ped_geometry_test_equal"
                                     '(* *))))
    (lambda (geometry-a geometry-b)
      (let ((res (proc (geometry->pointer geometry-a)
                       (geometry->pointer geometry-b))))
        (eq? res 1)))))

(define geometry-test-sector-inside?
  (let* ((proc (libparted->procedure int
                                     "ped_geometry_test_sector_inside"
                                     `(* ,sector-type))))
    (lambda (geometry sector)
      (let ((res (proc (geometry->pointer geometry) sector)))
        (eq? res 1)))))
