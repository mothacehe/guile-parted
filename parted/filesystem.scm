;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018, 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted filesystem)
  #:use-module (system foreign)
  #:use-module (parted bindings)
  #:use-module (parted structs)
  #:export (filesystem-type-register
            filesystem-type-unregister
            filesystem-type-alias-register
            filesystem-type-alias-unregister
            filesystem-type-get
            filesystem-type-get-next
            filesystem-types
            filesystem-alias-get-next
            filesystem-probe
            filesystem-probe-specific
            filesystem-open
            filesystem-close
            filesystem-resize
            filesystem-get-resize-constraint))

(define filesystem-type-register
  (let ((proc (libparted->procedure void
                                    "ped_file_system_type_register"
                                    '(*))))
    (lambda (filesystem-type)
      (proc (filesystem-type->pointer filesystem-type)))))

(define filesystem-type-unregister
  (let ((proc (libparted->procedure void
                                    "ped_file_system_type_unregister"
                                    '(*))))
    (lambda (filesystem-type)
      (proc (filesystem-type->pointer filesystem-type)))))

(define filesystem-type-alias-register
  (let ((proc (libparted->procedure void
                                    "ped_file_system_alias_register"
                                    `(* * ,int))))
    (lambda (filesystem-type alias deprecated)
      (proc (filesystem-type->pointer filesystem-type)
            (string->pointer alias)
            deprecated))))

(define filesystem-type-alias-unregister
  (let ((proc (libparted->procedure void
                                    "ped_file_system_alias_unregister"
                                    '(* *))))
    (lambda (filesystem-type alias)
      (proc (filesystem-type->pointer filesystem-type)
            (string->pointer alias)))))

(define filesystem-type-get
  (let ((proc (libparted->procedure '*
                                    "ped_file_system_type_get"
                                    '(*))))
    (lambda (name)
      (pointer->filesystem-type
       (proc (string->pointer name))))))

(define filesystem-type-get-next
  (let ((proc (libparted->procedure '*
                                    "ped_file_system_type_get_next"
                                    '(*))))
    (lambda* (#:key (filesystem-type #f))
      (let ((pointer (proc
                      (if filesystem-type
                          (filesystem-type->pointer filesystem-type)
                          %null-pointer))))
        (and (not (eq? pointer %null-pointer))
             (pointer->filesystem-type pointer))))))

(define (filesystem-types)
  (let loop ((type (filesystem-type-get-next)))
    (if (not type)
        '()
        (cons type
              (loop (filesystem-type-get-next
                     #:filesystem-type type))))))

(define filesystem-alias-get-next
  (let ((proc (libparted->procedure '*
                                    "ped_file_system_alias_get_next"
                                    '(*))))
    (lambda (filesystem-alias)
      (pointer->filesystem-alias
       (proc (filesystem-alias->pointer filesystem-alias))))))

(define filesystem-probe
  (let ((proc (libparted->procedure '*
                                    "ped_file_system_probe"
                                    '(*))))
    (lambda (geometry)
      (pointer->filesystem-type
       (proc (geometry->pointer geometry))))))

(define filesystem-probe-specific
  (let ((proc (libparted->procedure '*
                                    "ped_file_system_probe_specific"
                                    '(* *))))
    (lambda (filesystem-type geometry)
      (pointer->geometry
       (proc (filesystem-type->pointer filesystem-type)
             (geometry->pointer geometry))))))

(define filesystem-open
  (let ((proc (libparted->procedure '*
                                    "ped_file_system_open"
                                    '(*))))
    (lambda (geometry)
      (pointer->filesystem
       (proc (geometry->pointer geometry))))))

(define filesystem-close
  (let ((proc (libparted->procedure int
                                    "ped_file_system_close"
                                    '(*))))
    (lambda (filesystem)
      (proc (filesystem->pointer filesystem)))))

(define filesystem-resize
  (let ((proc (libparted->procedure int
                                    "ped_file_system_resize"
                                    '(* * *))))
    (lambda (filesystem geometry timer)
      (proc (filesystem->pointer filesystem)
            (geometry->pointer geometry)
            (timer->pointer timer)))))

(define filesystem-get-resize-constraint
  (let ((proc (libparted->procedure '*
                                    "ped_file_system_get_resize_constraint"
                                    '(*))))
    (lambda (filesystem)
      (pointer->constraint
       (proc (filesystem->pointer filesystem))))))
