;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018, 2019, 2022 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted device)
  #:use-module (system foreign)
  #:use-module (parted bindings)
  #:use-module (parted constraint)
  #:use-module (parted geom)
  #:use-module (parted natmath)
  #:use-module (parted structs)
  #:export (probe-all-devices!
            get-device
            get-device-next
            devices
            device-type
            device-type->string
            device-is-busy?
            device-open
            device-close
            device-cache-remove
            device-begin-external-access
            device-end-external-access
            device-sync
            device-sync-fast
            device-get-constraint
            device-get-minimal-aligned-constraint
            device-get-optimal-aligned-constraint
            device-get-minimum-alignment
            device-get-optimum-alignment))

;; Record all devices, so that we do not end up with different <device>
;; objects aliasing the same underlying C pointer. Use the pointer as key and
;; the associated <device> as value.
(define %devices (make-hash-table))

;; %DEVICES was a weak hash-table and we used to set a finalizer on POINTER.
;; This is inevitably causing double free issues for the following reason:
;;
;; When <device> goes out of scope and is removed from the %DEVICES table, the
;; finalizer that is set on the underlying C pointer is still registered but
;; possibly not called as finalization happens is a separate thread.  If a
;; subsequent call to ped_device_get returns the same C pointer, another
;; finalizer will be registered.  This means that the finalization function
;; can be called twice on the same pointer, causing a double free issue.
(define (pointer->device! pointer)
  (or (hash-ref %devices pointer)
      (let ((device (pointer->device pointer)))
        (hash-set! %devices pointer device)
        device)))

(define probe-all-devices!
  (let ((proc (libparted->procedure void
                                    "ped_device_probe_all"
                                    '())))
    (lambda ()
      (proc))))

(define get-device
  (let ((proc (libparted->procedure '*
                                    "ped_device_get"
                                    '(*))))
    (lambda (name)
      (pointer->device! (proc (string->pointer name))))))

(define get-device-next
  (let ((proc (libparted->procedure '*
                                    "ped_device_get_next"
                                    '(*))))
    (lambda* (#:key (device #f))
      (let ((pointer (proc (if device
                               (device->pointer device)
                               %null-pointer))))
        (and (not (eq? pointer %null-pointer))
             (pointer->device! pointer))))))

(define (devices)
  (let loop ((device (get-device-next)))
    (if (not device)
        '()
        (cons device (loop (get-device-next #:device device))))))

(define (device-type device)
  (case (device-raw-type device)
    ((0)  'device-type-unknown)
    ((1)  'device-type-scsi)
    ((2)  'device-type-ide)
    ((3)  'device-type-dac960)
    ((4)  'device-type-cpqarray)
    ((5)  'device-type-file)
    ((6)  'device-type-ataraid)
    ((7)  'device-type-i2o)
    ((8)  'device-type-ubd)
    ((9)  'device-type-dasd)
    ((10) 'device-type-viodasd)
    ((11) 'device-type-sx8)
    ((12) 'device-type-dm)
    ((13) 'device-type-xvd)
    ((14) 'device-type-sdmmc)
    ((15) 'device-type-virtblk)
    ((16) 'device-type-aoe)
    ((17) 'device-type-md)
    ((18) 'device-type-loop)
    ((19) 'device-type-nvme)
    ((20) 'device-type-ram)
    ((21) 'device-type-pmem)))

(define (device-type->string device-type)
  (case device-type
    ((device-type-unknown)  "unknown")
    ((device-type-scsi)     "scsi")
    ((device-type-ide)      "ide")
    ((device-type-dac960)   "dac960")
    ((device-type-cpqarray) "cpqarray")
    ((device-type-file)     "file")
    ((device-type-ataraid)  "ataraid")
    ((device-type-i2o)      "i2o")
    ((device-type-ubd)      "ubd")
    ((device-type-dasd)     "dasd")
    ((device-type-viodasd)  "viodasd")
    ((device-type-sx8)      "sx8")
    ((device-type-dm)       "dm")
    ((device-type-xvd)      "xvd")
    ((device-type-sdmmc)    "sd/mmc")
    ((device-type-virtblk)  "virtblk")
    ((device-type-aoe)      "aoe")
    ((device-type-md)       "md")
    ((device-type-loop)     "loopback")
    ((device-type-nvme)     "nvme")
    ((device-type-ram)      "brd")
    ((device-type-pmem)     "pmem")))

(define device-is-busy?
  (let ((proc (libparted->procedure int
                                    "ped_device_is_busy"
                                    '(*))))
    (lambda (device)
      (eq? (proc (device->pointer device)) 1))))

(define device-open
  (let ((proc (libparted->procedure int
                                    "ped_device_open"
                                    '(*))))
    (lambda (device)
      (proc (device->pointer device)))))

(define device-close
  (let ((proc (libparted->procedure int
                                    "ped_device_close"
                                    '(*))))
    (lambda (device)
      (proc (device->pointer device)))))

(define device-cache-remove
  (let ((proc (libparted->procedure void
                                    "ped_device_cache_remove"
                                    '(*))))
    (lambda (device)
      (proc (device->pointer device)))))

(define device-begin-external-access
  (let ((proc (libparted->procedure void
                                    "ped_device_begin_external_access"
                                    '(*))))
    (lambda (device)
      (proc (device->pointer device)))))

(define device-end-external-access
  (let ((proc (libparted->procedure void
                                    "ped_device_end_external_access"
                                    '(*))))
    (lambda (device)
      (proc (device->pointer device)))))

(define device-sync
  (let ((proc (libparted->procedure int
                                    "ped_device_sync"
                                    '(*))))
    (lambda (device)
      (proc (device->pointer device)))))

(define device-sync-fast
  (let ((proc (libparted->procedure int
                                    "ped_device_sync_fast"
                                    '(*))))
    (lambda (device)
      (proc (device->pointer device)))))

(define device-get-constraint
  (let ((proc (libparted->procedure '*
                                    "ped_device_get_constraint"
                                    '(*))))
    (lambda (device)
      (pointer->constraint!
       (proc (device->pointer device))))))

(define device-get-minimal-aligned-constraint
  (let ((proc (libparted->procedure '*
                                    "ped_device_get_minimal_aligned_constraint"
                                    '(*))))
    (lambda (device)
      (pointer->constraint!
       (proc (device->pointer device))))))

(define device-get-optimal-aligned-constraint
  (let* ((proc (libparted->procedure '*
                                    "ped_device_get_optimal_aligned_constraint"
                                    '(*))))
    (lambda (device)
      (let ((pointer (proc (device->pointer device))))
        (and (not (eq? pointer %null-pointer))
             (pointer->constraint! pointer))))))

(define device-get-minimum-alignment
  (let ((proc (libparted->procedure '*
                                    "ped_device_get_minimum_alignment"
                                    '(*))))
    (lambda (device)
      (pointer->alignment!
       (proc (device->pointer device))))))

(define device-get-optimum-alignment
  (let ((proc (libparted->procedure '*
                                    "ped_device_get_optimum_alignment"
                                    '(*))))
    (lambda (device)
      (pointer->alignment!
       (proc (device->pointer device))))))
