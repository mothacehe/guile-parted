;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018, 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted constraint)
  #:use-module (system foreign)
  #:use-module (parted bindings)
  #:use-module (parted structs)
  #:use-module (parted natmath)
  #:use-module (ice-9 match)
  #:export (pointer->constraint!
            constraint-init
            constraint-new
            constraint-new-from-min-max
            constraint-new-from-min
            constraint-new-from-max
            constraint-duplicate
            constraint-done
            constraint-intersect
            constraint-solve-max
            constraint-solve-nearest
            constraint-is-solution?
            constraint-any
            constraint-exact))

(define %constraint-destroy
  (libparted->pointer "ped_constraint_destroy"))

(define (pointer->constraint! pointer)
  (set-pointer-finalizer! pointer %constraint-destroy)
  (pointer->constraint pointer))

(define constraint-init
  (let ((proc (libparted->procedure int
                                    "ped_constraint_init"
                                    `(* * * * * ,sector-type ,sector-type))))
    (lambda* (constraint #:key
                         start-align
                         end-align
                         start-range
                         end-range
                         min-size
                         max-size)
      (proc (constraint->pointer constraint)
            (alignment->pointer start-align)
            (alignment->pointer end-align)
            (geometry->pointer start-range)
            (geometry->pointer end-range)
            min-size
            max-size))))

(define constraint-new
  (let* ((proc (libparted->procedure '*
                                     "ped_constraint_new"
                                     `(* * * * ,sector-type ,sector-type))))
    (lambda* (#:key
              start-align
              end-align
              start-range
              end-range
              min-size
              max-size)
      (let* ((start-alignment (match start-align
                                ('any (alignment-any))
                                ('none (alignment-none))
                                (else (alignment->pointer start-align))))
             (end-alignment (match end-align
                              ('any (alignment-any))
                              ('none (alignment-none))
                              (else (alignment->pointer end-align))))
             (pointer (proc start-alignment
                            end-alignment
                            (geometry->pointer start-range)
                            (geometry->pointer end-range)
                            min-size
                            max-size)))
        (and (not (eq? pointer %null-pointer))
             (pointer->constraint! pointer))))))

(define constraint-new-from-min-max
  (let ((proc (libparted->procedure '*
                                    "ped_constraint_new_from_min_max"
                                    '(* *))))
    (lambda* (#:key min max)
      (pointer->constraint!
       (proc (geometry->pointer min)
             (geometry->pointer max))))))

(define constraint-new-from-min
  (let ((proc (libparted->procedure '*
                                    "ped_constraint_new_from_min"
                                    '(*))))
    (lambda (min)
      (pointer->constraint!
       (proc (geometry->pointer min))))))

(define constraint-new-from-max
  (let ((proc (libparted->procedure '*
                                    "ped_constraint_new_from_max"
                                    '(*))))
    (lambda (max)
      (pointer->constraint!
       (proc (geometry->pointer max))))))

(define constraint-duplicate
  (let ((proc (libparted->procedure '*
                                    "ped_constraint_duplicate"
                                    '(*))))
    (lambda (constraint)
      (pointer->constraint!
       (proc (constraint->pointer constraint))))))

(define constraint-done
  (let ((proc (libparted->procedure void
                                    "ped_constraint_done"
                                    '(*))))
    (lambda (constraint)
      (proc (constraint->pointer constraint)))))

(define constraint-intersect
  (let* ((proc (libparted->procedure '*
                                    "ped_constraint_intersect"
                                    '(* *))))
    (lambda (constraint-a constraint-b)
      (let ((pointer (proc (constraint->pointer constraint-a)
                           (constraint->pointer constraint-b))))
        (and (not (eq? pointer %null-pointer))
             (pointer->constraint! pointer))))))

(define constraint-solve-max
  (let ((proc (libparted->procedure '*
                                    "ped_constraint_solve_max"
                                    '(*))))
    (lambda (constraint)
      (pointer->geometry
       (proc (constraint->pointer constraint))))))

(define constraint-solve-nearest
  (let ((proc (libparted->procedure '*
                                    "ped_constraint_solve_nearest"
                                    '(* *))))
    (lambda (constraint geometry)
      (pointer->geometry
       (proc (constraint->pointer constraint)
             (geometry->pointer geometry))))))

(define constraint-is-solution?
  (let* ((proc (libparted->procedure int
                                     "ped_constraint_is_solution"
                                     '(* *))))
    (lambda (constraint geometry)
      (let ((res
             (proc (constraint->pointer constraint)
                   (geometry->pointer geometry))))
        (eq? res 1)))))

(define constraint-any
  (let ((proc (libparted->procedure '*
                                    "ped_constraint_any"
                                    '(*))))
    (lambda (device)
      (pointer->constraint!
       (proc (device->pointer device))))))

(define constraint-exact
  (let ((proc (libparted->procedure void
                                    "ped_constraint_exact"
                                    '(*))))
    (lambda (geometry)
      (pointer->constraint!
       (proc (geometry->pointer geometry))))))
