;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018, 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted exception)
  #:use-module (system foreign)
  #:use-module (parted bindings)
  #:use-module (parted structs)
  #:export (EXCEPTION-TYPE-INFORMATION
            EXCEPTION-TYPE-WARNING
            EXCEPTION-TYPE-ERROR
            EXCEPTION-TYPE-FATAL
            EXCEPTION-TYPE-BUG
            EXCEPTION-TYPE-NO-FEATURE

            EXCEPTION-OPTION-UNHANDLED
            EXCEPTION-OPTION-FIX
            EXCEPTION-OPTION-YES
            EXCEPTION-OPTION-NO
            EXCEPTION-OPTION-OK
            EXCEPTION-OPTION-RETRY
            EXCEPTION-OPTION-IGNORE
            EXCEPTION-OPTION-CANCEL

            exception-get-type-string
            exception-get-option-string
            exception-set-handler
            exception-throw
            exception-rethrow
            exception-catch
            exception-fetch-all
            exception-reth
            exception-leave-all))

(define EXCEPTION-TYPE-INFORMATION 1)
(define EXCEPTION-TYPE-WARNING     2)
(define EXCEPTION-TYPE-ERROR       3)
(define EXCEPTION-TYPE-FATAL       4)
(define EXCEPTION-TYPE-BUG         5)
(define EXCEPTION-TYPE-NO_FEATURE  6)

(define EXCEPTION-OPTION-UNHANDLED 0)
(define EXCEPTION-OPTION-FIX       1)
(define EXCEPTION-OPTION-YES       2)
(define EXCEPTION-OPTION-NO        4)
(define EXCEPTION-OPTION-OK        8)
(define EXCEPTION-OPTION-RETRY     16)
(define EXCEPTION-OPTION-IGNORE    32)
(define EXCEPTION-OPTION-CANCEL    64)

(define exception-get-type-string
  (let ((proc (libparted->procedure '*
                                    "ped_exception_get_type_string"
                                    `(,int))))
    (lambda (exception-type)
      (pointer->string (proc exception-type)))))

(define exception-get-option-string
  (let ((proc (libparted->procedure '*
                                    "ped_exception_get_option_string"
                                    `(,int))))
    (lambda (exception-option)
      (pointer->string (proc exception-option)))))

(define exception-handler
  (lambda (callback)
    (procedure->pointer
     int
     (lambda (exception)
       (callback (pointer->exception exception)))
     '(*))))

(define exception-set-handler
  (let ((proc (libparted->procedure void
                                    "ped_exception_set_handler"
                                    '(*))))
    (lambda (handler)
      (proc (exception-handler handler)))))

(define exception-throw
  (let ((proc (libparted->procedure int
                                    "ped_exception_throw"
                                    `(,int ,int *))))
    (lambda (exception-type exception-option message)
      (proc exception-type
            exception-option
            (string->pointer message)))))

(define exception-rethrow
  (let ((proc (libparted->procedure int
                                    "ped_exception_rethrow"
                                    '())))
    (lambda ()
      (proc))))

(define exception-catch
  (let ((proc (libparted->procedure void
                                    "ped_exception_cancel"
                                    '())))
    (lambda ()
      (proc))))

(define exception-fetch-all
  (let ((proc (libparted->procedure void
                                    "ped_exception_fetch_all"
                                    '())))
    (lambda ()
      (proc))))

(define exception-leave-all
  (let ((proc (libparted->procedure void
                                    "ped_exception_leave_all"
                                    '())))
    (lambda ()
      (proc))))
