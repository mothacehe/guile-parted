;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018, 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted bindings)
  #:use-module (system foreign)
  #:use-module (parted config)
  #:export (libparted
            libparted->pointer
            libparted->procedure
            libparted->procedure*
            return-int->bool
            return-ptr->type))

(define (libparted->pointer name)
  (catch #t
    (lambda ()
      (dynamic-func name (dynamic-link %libparted)))
    (lambda args
      (lambda _
        (throw 'system-error name  "~A" (list (strerror ENOSYS))
               (list ENOSYS))))))

(define (libparted->procedure return name params)
  (catch #t
    (lambda ()
      (let ((ptr (dynamic-func name (dynamic-link %libparted))))
        ;; The #:return-errno? facility was introduced in Guile 2.0.12.
        (pointer->procedure return ptr params
                            #:return-errno? #t)))
    (lambda args
      (lambda _
        (throw 'system-error name  "~A" (list (strerror ENOSYS))
               (list ENOSYS))))))

(define-inlinable (libparted->procedure* name params)
  (let ((proc (libparted->procedure int name params)))
    (lambda args
      (apply proc args))))

(define (return-int->bool return)
  (eq? return 1))

(define* (return-ptr->type return
                           #:key (converter identity))
  (and (not (eq? return %null-pointer))
       (converter return)))
