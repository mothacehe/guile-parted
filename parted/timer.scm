;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2018, 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (parted timer)
  #:use-module (system foreign)
  #:use-module (parted bindings)
  #:use-module (parted structs)
  #:export (timer-handler
            timer-new
            timer-touch
            timer-reset
            timer-set-state-name))

(define %timer-destroy
  (libparted->pointer "ped_timer_destroy"))

(define (pointer->timer! pointer)
  (set-pointer-finalizer! pointer %timer-destroy)
  (pointer->timer pointer))

(define timer-handler
  (lambda (callback)
    (procedure->pointer
     void
     (lambda (timer context)
       (callback (pointer->timer timer) context))
     '(* *))))

(define timer-new
  (let ((proc (libparted->procedure '*
                                    "ped_timer_new"
                                    '(* *))))
    (lambda (handler context)
      (pointer->timer!
       (proc (timer-handler handler) context)))))

(define timer-touch
  (let ((proc (libparted->procedure void
                                    "ped_timer_touch"
                                    '(*))))
    (lambda (timer)
      (proc (timer->pointer timer)))))

(define timer-reset
  (let ((proc (libparted->procedure void
                                    "ped_timer_reset"
                                    '(*))))
    (lambda (timer)
      (proc (timer->pointer timer)))))

(define timer-set-state-name
  (let ((proc (libparted->procedure void
                                    "ped_timer_set_state_name"
                                    '(* *))))
    (lambda (timer state-name)
      (proc (timer->pointer timer)
            (string->pointer state-name)))))
