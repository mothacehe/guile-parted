(define-module (parted))

(eval-when (eval load compile)
  (begin
    (define %public-modules
      '((parted bindings)
        (parted constraint)
        (parted device)
        (parted disk)
        (parted exception)
        (parted filesystem)
        (parted geom)
        (parted natmath)
        (parted unit)
        (parted timer)
        (parted structs)))

    (for-each (let ((i (module-public-interface (current-module))))
                (lambda (m)
                  (module-use! i (resolve-interface m))))
              %public-modules)))
