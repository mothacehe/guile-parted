(use-modules (parted)
             (srfi srfi-1)
             (ice-9 match))

(probe-all-devices)

;; (define d (get-device "/gnu/store/rlfa4bbd2piy06fr5phhi8j5p7jc1wbi-disk-image"))
(define d (get-device "/home/mathieu/test.iso"))

(define (partition-end-formatted device partition)
  (unit-format-byte
   device
   (-
    (* (+ (partition-end partition) 1)
       (device-sector-size device))
    1)))

(define (print device)
  (let* ((type (device-type device))
         (path (device-path device))
         (model (device-model device))
         (type-str (device-type->string type))
         (default-unit (unit-get-default))
         (length (device-length device))
         (sector-size (device-sector-size device))
         (phys-sector-size (device-phys-sector-size device))
         (end (unit-format-byte d (* length sector-size))))
    (format #t "Model: ~a (~a)~%" model type-str)
    (format #t "Disk: ~a : ~a~%" path end)
    (format #t "Sector size (logical/physical): ~aB/~aB~%"
            sector-size phys-sector-size)
    (format #t "Partition table: ~a~%" (disk-type-name (disk-probe device))))

  (let* ((disk-type (disk-probe device))
         (disk (disk-new device))
         (has-extended? (disk-type-check-feature
                         disk-type
                         DISK-TYPE-FEATURE-EXTENDED))
         (has-name? (disk-type-check-feature
                     disk-type
                     DISK-TYPE-FEATURE-PARTITION-NAME))
         (partitions (disk-partitions disk)))
    (map (lambda (partition)
           (let ((type (partition-type partition)))
             (unless (eq? type 'metadata)
               (let* ((name (if has-name?
                                (if (eq? type 'free-space)
                                    "Free space"
                                    (partition-get-name partition))
                                ""))
                      (start (unit-format device
                                          (partition-start partition)))
                      (end (partition-end-formatted device partition))
                      (size (unit-format device (partition-length partition)))
                      (fs-type (partition-fs-type partition))
                      (fs-type-name (if fs-type
                                        (filesystem-type-name fs-type)
                                        ""))
                      (flags (if (eq? type 'free-space)
                                 ""
                                 (string-join
                                  (filter-map
                                   (lambda (flag)
                                     (and (> (partition-get-flag partition
                                                                 flag)
                                             0)
                                          (partition-flag-get-name flag)))
                                   (partition-flags partition))
                                  ",")))
                      (number (partition-number partition)))
                 (format #t "~a ~a ~a ~a ~a ~a ~a~%"
                         number start end size fs-type-name name flags)))))
         partitions)))

(define (mklabel device)
  (let* ((type (disk-type-get "msdos"))
         (disk (disk-new-fresh device type)))
    (disk-commit disk)))

(define* (mkpart device
                 #:key
                 name
                 (type PARTITION-TYPE-NORMAL)
                 fs-type
                 start end)

  (define (parse-start-end start end)
    (call-with-values
        (lambda ()
          (unit-parse start device))
      (lambda (start-sector start-range)
        (call-with-values
            (lambda ()
              (unit-parse end device))
          (lambda (end-sector end-range)
            (list start-sector start-range
                  end-sector end-range))))))

  (define (display-range range)
    (let ((start (geometry-start range))
          (end (geometry-end range))
          (length (geometry-length range)))
      (format #t "[~a;~a], length; ~a~%" start end length)))

  (define (extend-range range max-length)
    (let* ((end (geometry-end range))
           (new-end (+ end (/ MEBIBYTE-SIZE
                              (device-sector-size device)))))
      (and (< new-end max-length)
           (geometry-set-end range new-end))
      (display-range range)
      range))

  (match (parse-start-end start end)
    ((start-sector start-range end-sector end-range)
     (let* ((disk (disk-new device))
            (length (device-length device))
            (filesystem-type (filesystem-type-get fs-type))
            (partition (partition-new disk
                                      #:type type
                                      #:filesystem-type filesystem-type
                                      #:start start-sector
                                      #:end end-sector))
            (user-constraint (constraint-new
                              #:start-align 'any
                              #:end-align 'any
                              #:start-range (extend-range start-range length)
                              #:end-range end-range
                              #:min-size 1
                              #:max-size length))
            (dev-constraint
             (device-get-optimal-aligned-constraint device))
            (final-constraint (constraint-intersect user-constraint
                                                    dev-constraint))
            (no-constraint (constraint-any device))
            (partition-ok?
             (disk-add-partition disk partition final-constraint)))
       (when partition-ok?
         ;; (partition-set-name partition name)
         (partition-set-system partition filesystem-type)
         (disk-commit disk))))))

(define (rmpart device number)
  (let* ((disk (disk-new device))
         (partition (disk-get-partition disk number)))
    (disk-remove-partition disk partition)
    (disk-commit disk)))

(mklabel d)

(pk (mkpart d
            #:name "lol"
            #:type PARTITION-TYPE-EXTENDED
            #:fs-type "ext2"
            #:start "10.5MB"
            #:end "100MB"))
(pk (mkpart d
            #:name "lol2"
            #:type PARTITION-TYPE-LOGICAL
            #:fs-type "ext2"
            #:start "10MB"
            #:end "50MB"))
(pk (mkpart d
            #:name "lol3"
            #:type PARTITION-TYPE-LOGICAL
            #:fs-type "ext2"
            #:start "50MB"
            #:end "100MB"))
(pk (mkpart d
            #:name "lol4"
            #:type PARTITION-TYPE-NORMAL
            #:fs-type "ext2"
            #:start "100MB"
            #:end "200MB"))

(define disk (disk-new d))
(disk-delete-all disk)
(disk-commit disk)

;; (rmpart d 1)
;; (mklabel d)
;; (map test (devices))
