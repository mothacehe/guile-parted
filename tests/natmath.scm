;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests natmath)
  #:use-module (srfi srfi-64))

(use-modules (tests helpers))
(use-modules (parted))

(test-begin "natmath")

(define d (get-device (test-device "device.iso")))

(test-equal "round-up-to"
  12
  (round-up-to 11 3))

(test-equal "round-down-to"
  9
  (round-down-to 11 3))

(test-equal "round-to-nearest"
  12
  (round-to-nearest 11 3))

(test-equal "greatest-common-divisor"
  3
  (greatest-common-divisor 12 15))

(test-equal "alignment-init"
  3
  (greatest-common-divisor 12 15))

(test-assert "alignment-intersect"
  (let* ((align-a (alignment-new 11 3))
         (align-b (alignment-duplicate align-a)))
    (alignment-init align-b 11 2)
    (alignment-intersect align-a align-b)))

(test-end)
