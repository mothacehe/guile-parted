;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests constraint)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-64))

(use-modules (tests helpers))
(use-modules (parted))

(define (normal-partition-count disk)
  (let ((partitions (disk-partitions disk)))
    (count identity (map normal-partition? partitions))))

(define (extended-partition-count disk)
  (let ((partitions (disk-partitions disk)))
    (count identity (map extended-partition? partitions))))

(define (data-partition-count disk)
  (let ((partitions (disk-partitions disk)))
    (count identity (map data-partition? partitions))))

(define (equal-disks? . disks)
  (apply equal?
         (apply map
                (lambda partitions
                  (let ((check
                         (lambda (proc)
                           (apply equal? (map proc partitions)))))
                    (and
                     (check partition-type)
                     (check partition-start)
                     (check partition-end))))
                (map disk-partitions disks))))

(test-begin "disk")

(define d (get-device (test-device "device.iso")))

(test-equal "disk-probe"
  "msdos"
  (disk-type-name (disk-probe d)))

(test-assert "disk-type-check-feature - extended"
  (disk-type-check-feature (disk-probe d)
                           DISK-TYPE-FEATURE-EXTENDED))

(test-assert "disk-type-check-feature - partition name"
  (not
   (disk-type-check-feature (disk-probe d)
                            DISK-TYPE-FEATURE-PARTITION-NAME)))

(test-assert "disk-get-primary-partition-count"
  (let* ((disk (disk-new d))
         (count (disk-get-primary-partition-count disk)))
    (eq? count 1)))

(test-assert "disk-get-last-partition-num"
  (let* ((disk (disk-new d))
         (num (disk-get-last-partition-num disk)))
    (eq? num 1)))

(test-assert "disk-get-max-primary-partition-count"
  (let* ((disk (disk-new d))
         (count (disk-get-max-primary-partition-count disk)))
    (eq? count 4)))

(test-assert "disk-get-partitions"
  (let* ((disk (disk-new d))
         (partitions (disk-partitions disk)))
    (eq? (length partitions) 2)))

(test-assert "partition-type"
  (let* ((disk (disk-new d))
         (partitions (disk-partitions disk)))
    (equal? (map partition-type partitions)
            '((free-space) (normal)))))

(test-assert "partition-get-flag - boot"
  (let* ((disk (disk-new d))
         (partitions (disk-partitions disk))
         (boot-partition (find normal-partition? partitions)))
    (partition-get-flag boot-partition PARTITION-FLAG-BOOT)))

(test-assert "partition-get-flag - swap"
  (let* ((disk (disk-new d))
         (partitions (disk-partitions disk))
         (boot-partition (find normal-partition? partitions)))
    (not (partition-get-flag boot-partition PARTITION-FLAG-SWAP))))

(test-equal "partition-flag-get-name"
  "boot"
  (partition-flag-get-name PARTITION-FLAG-BOOT))

(test-equal "partition-flag-get-by-name"
  PARTITION-FLAG-BOOT
  (partition-flag-get-by-name "boot"))

(test-assert "partition-new"
  (with-tmp-device
   "device-raw.iso"
   (lambda (new-device)
     (let* ((device (get-device new-device))
            (type (disk-type-get "msdos"))
            (fs-type (filesystem-type-get "ext2"))
            (disk (disk-new-fresh device type))
            (partition (partition-new disk
                                      #:type PARTITION-TYPE-NORMAL
                                      #:filesystem-type fs-type
                                      #:start 1
                                      #:end (unit-parse "100%" device)))
            (no-constraint (constraint-any device)))
       ;; Try to stress the GC to check that device isn't destroyed.
       (gc)
       (disk-add-partition disk partition no-constraint)
       ;; Same as above.
       (gc)
       (partition-set-flag partition PARTITION-FLAG-BOOT 1)
       (equal-disks? disk (disk-new d))))))

(test-assert "partition-remove"
  (with-tmp-device
   "device.iso"
   (lambda (new-device)
     (let* ((device (get-device new-device))
            (disk (disk-new device))
            (_ (gc)) ; See above.
            (partitions (disk-partitions disk))
            (boot-partition (find normal-partition? partitions)))
       (gc)
       (disk-remove-partition disk boot-partition)
       (equal? (normal-partition-count disk) 0)))))

(test-assert "partition-remove extended"
  (with-tmp-device
   "device-extended.iso"
   (lambda (new-device)
     (let* ((device (get-device new-device))
            (disk (disk-new device))
            (partitions (disk-partitions disk))
            (extended-partition (find extended-partition? partitions)))
       (gc)
       (disk-remove-partition* disk extended-partition)
       (gc)
       (equal? (extended-partition-count disk) 0)))))

(test-assert "partition-remove-all"
  (with-tmp-device
   "device-extended.iso"
   (lambda (new-device)
     (let* ((device (get-device new-device))
            (disk (disk-new device)))
       (gc)
       (disk-remove-all-partitions disk)
       (gc)
       (equal? (data-partition-count disk) 0)))))

(test-end)
