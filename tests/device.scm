;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests device)
  #:use-module (srfi srfi-64))

(use-modules (tests helpers))
(use-modules (parted))

(test-begin "device")

(define d (get-device (test-device "device.iso")))

(test-equal "device-type"
    'device-type-file
    (device-type d))

(test-equal "device-is-busy?"
  #f
  (device-is-busy? d))

(test-equal "device-length"
  6144
  (device-length d))

(test-equal "device-sector-size"
  512
  (device-sector-size d))

(test-end)
