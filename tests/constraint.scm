;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests constraint)
  #:use-module (srfi srfi-64))

(use-modules (tests helpers))
(use-modules (parted))

(test-begin "constraint")

(define d (get-device (test-device "device.iso")))

(define* (make-constraint #:key
                          start
                          end
                          (offset 3)
                          (min-size 1)
                          (max-size 50))
  (let ((start-range (geometry-new d #:start start #:length offset))
        (end-range (geometry-new d #:start end #:length offset)))
    (constraint-new #:start-align 'any
                    #:end-align 'any
                    #:start-range start-range
                    #:end-range end-range
                    #:min-size min-size
                    #:max-size max-size)))

(test-assert "constraint-new"
  (let* ((constraint (make-constraint #:start 1 #:end 10))
         (geometry (geometry-new d #:start 1 #:length 12)))
    (constraint-is-solution? constraint geometry)))

(test-assert "constraint-intersect & constraint-any"
  (let* ((constraint (make-constraint #:start 1 #:end 10))
         (intersect (constraint-intersect constraint
                                          (constraint-any d)))
         (geometry (geometry-new d #:start 1 #:length 12)))
    (constraint-is-solution? intersect geometry)))

(test-assert "constraint-intersect"
  (let* ((constraint-a (make-constraint #:start 1 #:end 10))
         (constraint-b (make-constraint #:start 3 #:end 8))
         (intersect (constraint-intersect constraint-a constraint-b))
         (geometry (geometry-new d #:start 3 #:length 8)))
    (constraint-is-solution? intersect geometry)))

(test-end)
