;;; Guile-parted --- GNU Guile bindings of libparted
;;; Copyright © 2019 Mathieu Othacehe <m.othacehe@gmail.com>
;;;
;;; This file is part of Guile-parted.
;;;
;;; Guile-parted is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guile-parted is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; long with Guile-parted.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tests geom)
  #:use-module (srfi srfi-64))

(use-modules (tests helpers))
(use-modules (parted))

(test-begin "geom")

(define d (get-device (test-device "device.iso")))
(define g (geometry-new d #:start 0 #:length 1))

(unit-set-default UNIT-KILOBYTE)

(test-assert "geometry-init"
  (begin
    (geometry-init g d #:start 1 #:length 100)
    (and (eq? (geometry-start g) 1)
         (eq? (geometry-end g) 100))))

(test-assert "geometry-duplicate & geometry-test-equal?"
  (let ((new-geom (geometry-duplicate g)))
    (geometry-test-equal? g new-geom)))

(test-assert "geometry-intersect"
  (let* ((new-geom (geometry-new d #:start 50 #:length 100))
         (intersect-geom (geometry-intersect g new-geom)))
    (and (eq? (geometry-start intersect-geom) 50)
         (eq? (geometry-end intersect-geom) 100))))

(test-assert "geometry-set"
  (begin
    (geometry-set g #:start 10 #:length 10)
    (and (eq? (geometry-start g) 10)
         (eq? (geometry-end g) 19))))

(test-assert "geometry-set-start"
  (begin
    (geometry-set-start g 15)
    (eq? (geometry-start g) 15)))

(test-assert "geometry-set-end"
  (begin
    (geometry-set-end g 20)
    (eq? (geometry-end g) 20)))

(test-assert "geometry-test-overlap?"
  (let ((geom-a (geometry-new d #:start 50 #:length 20))
        (geom-b (geometry-new d #:start 50 #:length 100)))
    (geometry-test-overlap? geom-a geom-b)))

(test-assert "geometry-test-inside?"
  (let ((geom-a (geometry-new d #:start 50 #:length 100))
        (geom-b (geometry-new d #:start 50 #:length 20)))
    (geometry-test-inside? geom-a geom-b)))

(test-assert "geometry-test-sector-inside?"
  (let ((geom (geometry-new d #:start 50 #:length 100)))
    (geometry-test-sector-inside? geom 60)))

(test-end)
